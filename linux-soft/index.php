<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("linux-soft");
?>

<?//Комплексный компонент?>
<?$APPLICATION->IncludeComponent(
	"custom:custom.complex", 
	".default", 
	array(
		"IBLOCK_ID" => "4",
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "Linux_Soft",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/linux-soft/",
		"SEF_URL_TEMPLATES" => array(
			"element_list" => "",
			"section" => "",
			"element" => "#ELEMENT_CODE#/",
		)
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
