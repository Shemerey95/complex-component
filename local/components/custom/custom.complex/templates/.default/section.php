<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$APPLICATION->IncludeComponent(
    "custom:custom.section.list",
    ".default",
    array(
        "IBLOCK_ID" => "4",
        "COMPONENT_TEMPLATE" => ".default",
        "IBLOCK_TYPE" => "Linux_Soft",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
    ),
    $component
);?>