<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */

$this->setFrameMode(true);
?>

<div class="content">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <div class="content__element">
            <a class="content__element_link-name" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                <h3><?=$arItem["NAME"]?></h3>
            </a>
            <a class="content__element_link-image" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                <img src="<?=$arItem["PREVIEW_PICTURE_SRC_SMALL"]?>" alt="<?=$arItem["NAME"]?>">
            </a>
            <h4 class="content__element_description-title">Описание:</h4>
            <p class="content__element_description"><?=$arItem["PREVIEW_TEXT"]?></p>
        </div>
    <?endforeach;?>
</div>






