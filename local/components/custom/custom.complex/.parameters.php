<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/**
 * @var string $componentPath
 * @var string $componentName
 * @var array $arCurrentValues
 * */

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

if( !Loader::includeModule("iblock") ) {
    throw new \Exception('Не загружены модули необходимые для работы компонента');
}

// типы инфоблоков
$arIBlockType = CIBlockParameters::GetIBlockTypes();

// инфоблоки выбранного типа
$arIBlock = [];
$iblockFilter = !empty($arCurrentValues['IBLOCK_TYPE'])
    ? ['TYPE' => $arCurrentValues['IBLOCK_TYPE'], 'ACTIVE' => 'Y']
    : ['ACTIVE' => 'Y'];

$rsIBlock = CIBlock::GetList(['SORT' => 'ASC'], $iblockFilter);
while ($arr = $rsIBlock->Fetch()) {
    $arIBlock[$arr['ID']] = '['.$arr['ID'].'] '.$arr['NAME'];
}
unset($arr, $rsIBlock, $iblockFilter);

$arComponentParameters = [
    "GROUPS" => [
        "SETTINGS" => [
            "NAME" => Loc::getMessage("EXAMPLE_COMPSIMPLE_PROP_SETTINGS"),
            "SORT" => 490,
        ],
    ],
    
    "PARAMETERS" => [
        "IBLOCK_TYPE" => [
            "PARENT" => "SETTINGS",
            "NAME" => Loc::getMessage("EXAMPLE_COMPSIMPLE_PROP_IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arIBlockType,
            "REFRESH" => "Y"
        ],
        "IBLOCK_ID" => [
            "PARENT" => "SETTINGS",
            "NAME" => Loc::getMessage("EXAMPLE_COMPSIMPLE_PROP_IBLOCK_ID"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arIBlock,
            "REFRESH" => "Y"
        ],
        "SEF_MODE" => [
            "element.list" => [
                "NAME" => GetMessage("EXAMPLE_COMPSIMPLE_PROP_ELEMENT_LIST"),
                "DEFAULT" => "",
                "VARIABLES" => [],
            ],
            "section" => [
                "NAME" => GetMessage("EXAMPLE_COMPSIMPLE_PROP_ELEMENT_SECTION"),
                "DEFAULT" => "",
                "VARIABLES" => ["SECTION_ID"],
            ],
            "element" => [
                "NAME" => GetMessage("EXAMPLE_COMPSIMPLE_PROP_ELEMENT_ID"),
                "DEFAULT" => "#ELEMENT_ID#/",
                "VARIABLES" => ["ELEMENT_ID", "SECTION_ID"],
            ],
        ],
        'CACHE_TIME' => ['DEFAULT' => 3600],
    ]
];