<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true){die();}
use Bitrix\Main\Application;
use Bitrix\Iblock\ElementTable;

class Vacancy extends \CBitrixComponent
{
    public function executeComponent()
    {
        
        $arVariables    = [];
        $arUrlTemplates = [
            "element.list"  => "",
            "element" => "#ELEMENT_ID#/",
        ];
        
        
        $folder = 'linux-soft/';
        
        $engine        = new \CComponentEngine($this);
        $componentPage = $engine->ParseComponentPath(
            $folder,
            $arUrlTemplates,
            $arVariables
        );
        
        $this->arResult['SEF_VARIABLES'] = $arVariables;
        if (strlen($componentPage) <= 0) {
            $componentPage = "element.list";
        }
        
        $this->arResult['VARIABLES'] = $arVariables;
        
        $this->IncludeComponentTemplate($componentPage);
    }
}

